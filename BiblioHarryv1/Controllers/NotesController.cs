﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BiblioHarryv1.Models;
using System.Text;

namespace BiblioHarryv1.Controllers
{
    public class NotesController : Controller
    {
        private BiblioNotesEntities db = new BiblioNotesEntities();

        // GET: /Notes/
        public ActionResult Index()
        {
            return View(db.Notes.ToList());
        }

        // GET: /Notes/Details/5
        public ActionResult Details(Guid? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }


        //GET
        public ActionResult ShowDuplicates(string origkey)
        {
            if (origkey != "")
            {
                string[] ArrList = origkey.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                int ListCount = ArrList.Length;
                StringBuilder DupList = new StringBuilder();

                if (ListCount > 0)
                {
                    DupList.Clear();
                    Array.Sort(ArrList);
                    for (int i = 1; i <= ListCount - 1; i++)
                    {
                        //check for duplicate
                        if (ArrList[i] == ArrList[i - 1])
                        {
                            DupList.AppendLine(ArrList[i]);
                        }
                    }
                }
                ViewBag.DupKeyword = DupList.ToString().Replace("\n", "<br />").Replace("\r", null);
                ViewBag.RawKeyword = DupList.ToString();
            }            
            return View();
        }

        // GET: /Notes/Create
        public ActionResult Create()
        {          

            return View();
        }

        //function to reverse string
        public string ReverseString (string inputString)
        {            
            char[] arrString = inputString.ToCharArray();
            Array.Reverse(arrString);
            string outString = new string(arrString);
            return outString;
        }

        public ActionResult SaveFile(string sText)
        {
            string filename = "bibliotheca" + System.DateTime.Now;
            Response.Clear();
            Response.ContentType = "text/html";
            Response.AddHeader("content-disposition", "attachment; filename=\"" + filename +".txt\"");
            Response.Write(sText);
            Response.End();
            return View();
        }
        // POST: /Notes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /*public ActionResult Create(FormCollection form)
         string temptext = "";
            temptext = form["notetext"].ToString();
         */

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Note model)
        {
            string temptext = "";
            if (ModelState.IsValid)
            {
                temptext = model.NoteList;
                string[] ArrList = temptext.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                int ListCount = ArrList.Length;
                string sPalin = "";
                StringBuilder PalinList = new StringBuilder();

                if (ListCount > 0)
                {
                    PalinList.Clear();

                    for (int i = 0; i < ListCount; i++)
                    {
                        sPalin = ReverseString(ArrList[i]);
                        //check for palin
                        if (sPalin == ArrList[i])
                        {
                            PalinList.AppendLine(sPalin);
                        }
                    }
                }
                ViewBag.RawPalin = PalinList.ToString();
                ViewBag.Palintext = PalinList.ToString().Replace("\n", "<br />").Replace("\r", null);
                ViewBag.OrigKeyword = temptext;
            }

            return View(model);
        }

        // GET: /Notes/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        // POST: /Notes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="NoteID,NoteList,Palindrome,Duplicates,EntryDate")] Note note)
        {
            if (ModelState.IsValid)
            {
                db.Entry(note).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(note);
        }

        // GET: /Notes/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        // POST: /Notes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Note note = db.Notes.Find(id);
            db.Notes.Remove(note);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
